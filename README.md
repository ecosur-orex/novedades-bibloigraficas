# Novedades-bibloigraficas (Carrusel de novedades)

![Screenshot](screnshot.png)

***Demos:***
- Pagina principal de SBIE - https://sibeservicios.ecosur.mx/sibe
- Pagina principal de catálogo https://biblioteca.ecosur.mx/cgi-bin/koha/opac-main.pl

## Requerimientos
---
1. Generar un informe SQL en Koha:

	En primer lugar, debe crear uno o más informes públicos en los que se basara el widgets. 
	Es importante que tenga un isbn bueno y válido, ya que ese es el dato utilizado para buscar la portada.

	Ejemplo SQL de los últimos libros registrados:
	```sql
	SELECT 
		DISTINCT biblio.title, 
		biblio.biblionumber,  
		SUBSTRING_INDEX(biblioitems.isbn, ' ', 1) AS isbn, 
		c.imagenumber AS localcover 
	FROM items 
		LEFT JOIN biblioitems USING (biblioitemnumber) 
		LEFT JOIN biblio USING biblionumber
		LEFT JOIN cover_images c USING biblionumber
	WHERE 
		biblioitems.isbn IS NOT NULL 
		AND biblioitems.isbn !=''
	ORDER  BY biblionumber desc
	LIMIT  15
	``` 

	Esto generara un url API-SVC
		
		https://library/cgi-bin/koha/svc/report?id={id-reporte}


## Uso
--- 

**En wordpress:**

1. Instalar el plugin de manera normal
2. Agregar el wdget en la seccion de Widget de Wrodpress y colocar la URL-API
	![widget](wdget_wordpress.png) 


**En koha:**

1. Debe estar ya cargado el plugin en Wrodpress
2. Agregar en la sección de Noticias Koha el siguiente código:
	```html
	<!-- OpacMainUseBlock -->
	<div class="wp-novedades-bibliograficas-sibe" data-object-id="wpr_object_613fa332adb48"></div>
	
	<!-- opaccredits -->
	<link href="https://url-wordpress/wp-content/plugins/wp-novedades-bibliograficas-sibe/novedades-bibliograficas-sibe-react/dist/css/widget.css?ver=5.8.1" rel="stylesheet" />
	<script type="text/javascript" src="https://url-wordpress/wp-content/plugins/wp-novedades-bibliograficas-sibe/novedades-bibliograficas-sibe-react/dist/widget.js?ver=1.0.0" id="wp-novedades-bibliograficas-sibe-widget-script-wnb-js"></script>
	```

## Instalación y modificaciones en dev
```bash
# Instalar dependencias del componente
cd novedades-bibliograficas-sibe-react 
npm install

#compilar componente
npm run build
```
## Creditos

- [Saul Oswaldo Lara Gonzalez](https://gitlab.com/solg713)