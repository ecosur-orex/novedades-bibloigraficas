<?php
/**
 * WP-NovedadesBibliograficas
 *
 *
 * @package   WP-NovedadesBibliograficas
 * @author    Germán Hernandez
 * @license   GPL-3.0
 * @link      https://gopangolin.com
 * @copyright 2022 SIBE Ltd
 */

namespace gjhernandez1234\NovedadesBibliograficasSibe;

/**
 * @subpackage Shortcode
 */
class Shortcode {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
			self::$instance->do_hooks();
		}

		return self::$instance;
	}

	/**
	 * Initialize the plugin by setting localization and loading public scripts
	 * and styles.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {
		$plugin = Plugin::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();
		$this->version = $plugin->get_plugin_version();

		add_shortcode( 'novedades-bibliograficas-sibe', array( $this, 'shortcode' ) );
	}


	/**
	 * Handle WP actions and filters.
	 *
	 * @since 	1.0.0
	 */
	private function do_hooks() {
		add_action( 'wp_enqueue_scripts', array( $this, 'register_frontend_scripts_2' ) );
	}

	/**
	 * Register frontend-specific javascript
	 *
	 * @since     1.0.0
	 */
	public function register_frontend_scripts_2() {
		wp_register_script( $this->plugin_slug . '-shortcode-script-novedades', plugins_url( 'novedades-bibliograficas-sibe-react/dist/shortcode.js', dirname( __FILE__ ) ), array( 'jquery' ), $this->version,true );
		wp_register_style( $this->plugin_slug . '-shortcode-style-novedades', plugins_url( 'novedades-bibliograficas-sibe-react/dist/css/shortcode.css', dirname( __FILE__ ) ), $this->version );
	}

	public function shortcode( $atts ) {
		wp_enqueue_script( $this->plugin_slug . '-shortcode-script-novedades' );
		wp_enqueue_style( $this->plugin_slug . '-shortcode-style-novedades' );

		$object_name = 'wpr_object_' . uniqid();

		$object = shortcode_atts( array(
			'titulo'       => '',
			'url' => ''
		), $atts, 'novedades-bibliograficas-sibe' );

		wp_localize_script( $this->plugin_slug . '-shortcode-script-novedades', $object_name, $object );

		$shortcode = '<div class="wp-novedades-bibliograficas-sibe-shortcode" data-object-id="' . $object_name . '"></div>';
		return $shortcode;
	}
}
