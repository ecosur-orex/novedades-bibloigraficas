<?php
/**
 * WP-NovedadesBibliograficas
 *
 *
 * @package   WP-NovedadesBibliograficas
 * @author    Germán Hernandez
 * @license   GPL-3.0
 * @link      https://gopangolin.com
 * @copyright 2022 SIBE Ltd
 */

namespace gjhernandez1234\NovedadesBibliograficasSibe;

/**
 * @subpackage Widget
 */
class Widget extends \WP_Widget {

	/**
	 * Initialize the widget
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		$plugin = Plugin::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();
		$this->version = $plugin->get_plugin_version();

		$widget_ops = array(
			'classname' => 'novedades-bibliograficas-sibe-widget',
			'description' => esc_html__( 'Carrusel (flipster) de libros.', $this->plugin_slug ),
		);

		parent::__construct( 'novedades-bibliograficas-sibe-widget', esc_html__( 'Novedades bibliograficas SIBE', $this->plugin_slug ), $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		wp_enqueue_script( $this->plugin_slug . '-widget-script-wnb', plugins_url( 'novedades-bibliograficas-sibe-react/dist/widget.js',
		                       dirname( __FILE__ ) ), array( 'jquery' ), $this->version );
		                       
		wp_enqueue_style( $this->plugin_slug . '-widget-style-wnb', plugins_url( 'novedades-bibliograficas-sibe-react/dist/css/widget.css',
		                      dirname( __FILE__ ) ), $this->version );
		

		$object_name = 'wpr_object_' . uniqid();
		$object = array(
			'url'         => $instance['url'],
		);

		wp_localize_script( $this->plugin_slug . '-widget-script-wnb', $object_name, $object );

		echo $args['before_widget'];

		?>
		<div class="container">
			<div class="wp-novedades-bibliograficas-sibe" data-object-id="<?php echo $object_name ?>"></div>
		</div>
		<?php

		echo $args['after_widget'];
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance
	 * @return string|void
	 */
	public function form( $instance ) {
		$url = ( ! empty( $instance['url'] ) ) ? $instance['url'] : '';
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'url' ); ?>">
				<?php esc_html_e( 'Url (Datos):', 'wp-reactivate' ); ?>
			</label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'url' ); ?>" name="<?php echo $this->get_field_name( 'url' ); ?>"
					type="text" value="<?php echo esc_attr( $url ); ?>" />
		</p>
		<?php
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

        $instance['url'] = sanitize_text_field( $new_instance['url'] );
		
        return $instance;
	}
}
