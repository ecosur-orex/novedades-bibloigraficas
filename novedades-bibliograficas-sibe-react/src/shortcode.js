import React from 'react';
import ReactDOM from 'react-dom';
import NovedadesSIBE from './components/novedades-bibliograficas-sibe.jsx';

document.addEventListener('DOMContentLoaded', function() {
  const widget_containers = document.querySelectorAll('.wp-novedades-bibliograficas-sibe-shortcode');

  for (let i = 0; i < widget_containers.length; ++i) {
    const objectId = widget_containers[i].getAttribute('data-object-id');

    ReactDOM.render(<NovedadesSIBE wpObject={window[objectId]} />, widget_containers[i]);
  }
});

