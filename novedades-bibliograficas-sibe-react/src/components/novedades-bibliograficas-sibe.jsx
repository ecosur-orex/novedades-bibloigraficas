import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../css/style.scss';
import '../css/Cargando.css';
import 'jquery.flipster/dist/jquery.flipster.min.js';
import 'jquery.flipster/dist/jquery.flipster.min.css';
import noImage from '../images/no-image.png';

export default class NovedadesSIBE extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      isExec: false,
      items: []
    };
  }
  
  componentDidUpdate(){
    const { isLoaded, isExec } = this.state;
    if (isLoaded) {
      this.state.isExec = true
    }

    if (isLoaded && !isExec) {
        $( document ).ready(function() {
            $(".coverflow").flipster({scrollwheel:false});
        });
        $( window ).on( "load", function () {
            $(".coverflow").flipster({scrollwheel:false});
        });
    }
  }
  
  componentDidMount() {
    fetch(this.props.wpObject.url)
      .then(res => res.json())
      .then(
        (data) => {
          this.setState({
            isLoaded: true,
            items: data
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, items } = this.state;
    let titulo="";
    if(this.props.wpObject && this.props.wpObject.titulo && this.props.wpObject.titulo !== '') {
      titulo  = <h2 class="titulos-secciones">{this.props.wpObject.titulo}</h2>
    }
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <Cargando />;
    } else {
      return (
        <div>
          {titulo}
          <div className="coverflow">
            <ul class="flip-items">
              {items.map((item, index) => {
                  let imagen = noImage;
                  let url = "https://biblioteca.ecosur.mx/cgi-bin/koha/opac-detail.pl?biblionumber="+item[1];
                  if (item[3] > 0){
                      imagen = "https://biblioteca.ecosur.mx/cgi-bin/koha/opac-image.pl?biblionumber="+item[1];
                  }else{
                    const image = new Image();
                    image.onload = () => {
                      if (image.naturalHeight == 1){ 
                        $(".img"+item[1]).attr('src',noImage); 
                      } 
                    };
                    image.src = "https://images-na.ssl-images-amazon.com/images/P/"+item[2]+".01.LZZZZZZZ.jpg";
                    imagen = "https://images-na.ssl-images-amazon.com/images/P/"+item[2]+".01.LZZZZZZZ.jpg";
                  }
                  
                  return <li data-flip-title={item[0]}>
                    <a className="elemento-nobi" href={url} target="_blank">
                      <img className={"imagenCarrusel img"+item[1]} src={imagen} title={item[0].substr(0, 60)+"..."}/>
                      <p>{item[0].substr(0, 60)}...</p>
                    </a>
                  </li>
                }
              )}
            </ul>
          </div>
        </div>
      );
    }
  }
}

class Cargando extends Component {
    render(){
        return(
            <div className="spinner-nobi-sibe">
              <div className="rect1"></div>
              <div className="rect2"></div>
              <div className="rect3"></div>
              <div className="rect4"></div>
              <div className="rect5"></div>
            </div>
        );
    }
}

NovedadesSIBE.propTypes = {
  wpObject: PropTypes.object
};

