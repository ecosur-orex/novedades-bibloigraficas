<?php
/**
 * SIBE-Novedades-bibliograficas
 *
 *
 * @package   SIBE-Novedades-bibliograficas
 * @author    Germán Hernandez
 * @license   GPL-3.0
 * @link      https://gopangolin.com
 * @copyright 2022 SIBE
 *
 * @wordpress-plugin
 * Plugin Name:       SIBE-Novedades-bibliograficas
 * Description:       Componente de novedades bibliograficas obtenidas de Koha
 * Version:           2.0.0
 * Author:            German Hernandez
 * Author URI:        https://gopangolin.com
 * Text Domain:       wp-reactivate
 * License:           GPL-3.0
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.txt
 * Domain Path:       /languages
 */


namespace gjhernandez1234\NovedadesBibliograficasSibe;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'WP_NOVEDADESBIBLIOGRAFICAS_VERSION', '6.0.2' );
define('NOVEDADESBIBLIO_RUTA',plugin_dir_path(__FILE__));

/**
 * Autoloader
 *
 * @param string $class The fully-qualified class name.
 * @return void
 *
 *  * @since 1.0.0
 */
spl_autoload_register(function ($class) {

    // project-specific namespace prefix
    $prefix = __NAMESPACE__;

    // base directory for the namespace prefix
    $base_dir = __DIR__ . '/includes/';

    // does the class use the namespace prefix?
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        // no, move to the next registered autoloader
        return;
    }

    // get the relative class name
    $relative_class = substr($class, $len);

    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

    // if the file exists, require it
    if (file_exists($file)) {
        require $file;
    }
});

/**
 * Initialize Plugin
 *
 * @since 1.0.0
 */
function init() {
	$wpr = Plugin::get_instance();
	$wpr_shortcode = Shortcode::get_instance();
}
add_action( 'plugins_loaded', 'gjhernandez1234\\NovedadesBibliograficasSibe\\init' );



/**
 * Register the widget
 *
 * @since 1.0.0
 */
function widget_init() {
	return register_widget( new Widget );
}
add_action( 'widgets_init', 'gjhernandez1234\\NovedadesBibliograficasSibe\\widget_init' );

/**
 * Register activation and deactivation hooks
 */
register_activation_hook( __FILE__, array( 'gjhernandez1234\\NovedadesBibliograficasSibe\\Plugin', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'gjhernandez1234\\NovedadesBibliograficasSibe\\Plugin', 'deactivate' ) );

